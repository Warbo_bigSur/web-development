const express = require("express");
const bodyParser = require("body-parser");
const _ = require("lodash");
const app = express();
const mongoose = require("mongoose");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));
mongoose.connect("mongodb+srv://amin-tri:test124@cluster0.4fadj.mongodb.net/todoListDB", { useNewUrlParser: true });
const itemsSchema = {
  
  name: String

}
const listSchema = {
  name: String,
  items: [itemsSchema]
}

const Item = mongoose.model("Item", itemsSchema)
const List = mongoose.model("List", listSchema)
const Chicken = new Item({
  name: "Chicken",

})
const Food = new Item({
  name: " Food",

})
const Strawbery = new Item({
  name: "Strawbery",

})
const defaultItem = [Food, Strawbery, Chicken]

app.set("view engine", "ejs");
app.get("/", function (req, res) {
  Item.find({}, function (err, results) {
    if (results.length === 0) {
      Item.insertMany(defaultItem, function (err) {
        if (err) {
          console.log(err)
        } else {
          console.log("Successfully Iserted")
        }
        res.redirect("/")
      })
    } else {
      res.render("list", { listTitle: "Food List", newItems: results })
    }

  })

})
app.get("/:customListName", function (req, res) {
  const customListName = _.capitalize(req.params.customListName) 
  List.findOne({ name: customListName }, function (err, foundList) {
    if (!err) {
      if (!foundList) {
        const list = new List({
          name: customListName,
          items: defaultItem
        })
        list.save();
        res.redirect("/" + customListName);
      } else {
        res.render("list", { listTitle: foundList.name, newItems: foundList.items })
      }
    }
  });

})


app.get("/about", function (req, res) {
  res.render("about")

})
app.post("/", function (req, res) {
  const itemName = req.body.addItem;
  const listName = req.body.list;

  const item = new Item({
    name: itemName
  });
  if (listName === "Food List") {
    item.save()
    res.redirect("/")
  } else {
    List.findOne({ name: listName }, function (err, foundList) {
      foundList.items.push(item)
      foundList.save()
      res.redirect("/" + listName);

    })
  }





})
app.post("/delete", function (req, res) {
  const checkedItemId = req.body.checkbox;
  const listName = req.body.listName;
  if (listName === "Food List") {
    Item.findByIdAndRemove(checkedItemId, function (err) {
      if (err) {
        console.log(err)
      } else {
        console.log("Deleted")
        res.redirect("/")
      }
    });
  } else {
    List.findOneAndUpdate({ name: listName }, {$pull: {items: { _id: checkedItemId }}}, function (err, foundList) {
      if (!err) {
        res.redirect("/" + listName);
      }else{
        console.log(err)
      }

    })
  }

});
let port = process.env.PORT;
if (port == null || port == "") {
  port = 3000;
}

app.listen(port, console.log("started successfully"));